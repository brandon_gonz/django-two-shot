from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User


class LoginForm(ModelForm):
    username = forms.CharField(max_length=150, required=True)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            "username",
            "password",
        ]
