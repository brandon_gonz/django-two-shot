from django.shortcuts import redirect, render
from accounts.forms import LoginForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.


def user_login(request):
    if request.method == "POST":
        print("POST REQUEST")
        form = LoginForm(request.POST)
        if form.is_valid():
            print("if form is valid")
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                print("if form is REALLY valid")
                login(request, user)
                print("redirecting to recepirs detail html")
                # return redirect("home")
                return render(request, "receipts/detail.html", context)

    else:
        print("non-Post REQUEST")
        form = LoginForm()
    context = {
            "form": form,
        }
    return render(request, "accounts/login.html", context)

# def logout_view(request):
#     logout(request)
#     return redirect("recipe_list")
