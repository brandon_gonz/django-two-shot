from django.urls import path,reverse_lazy
from accounts.views import user_login
from django.views.generic.base import RedirectView


urlpatterns = [
    path('', lambda request: redirect('receipts/detal.html'), name='accounts_home_redirect'),
    path("login/", user_login, name="login"),
    path('', RedirectView.as_view(url='/receipts/', permanent=True)),
    # path("", RedirectView.as_view(url=reverse_lazy("home"))),
]
